# Service

This repository is part of the DevOps Ansible + Terraform project.

The code prepares a test microservice for use in the AWS infrastructure.

- [Runtime](#runtime)
- [Configure the GitLab Repository](#configure-the-gitlab-repository)
- [How to work with an application](#how-to-work-with-an-application)
  - [Without Docker](#without-docker)
  - [With Docker](#with-docker)

# Runtime

There are 3 endpoints:

- `/` – returns a part of the request and generates a log line
- `/health` – 200
- `/metrics` – Prometheus metrics

# Configure the GitLab Repository

All manual steps that will be described in the section below will be automatically reproduced in GitLab CI/CD, thus, it is necessary to specify the following values in the variables for the runner:

- `PROD_KEY` - to access prod host
- `TEST_KEY` - to access test host
- `APP_HOST_TEST` – to access prod host
- `APP_HOST_PROD` - to access test service
- `CI_REGISTRY_NAME` - to access docker registry
- `CI_REGISTRY_PASS` - to access docker registry

# How to work with an application

### Without Docker

1. Install `golang 1.16+`
2. Install dependencies:
   ```bash
   go mod download
   ```
3. Run tests:
   ```bash
   go test -v ./...
   ```
4. Build application:
   ```bash
   GO111MODULE=on go build -o app cmd/server/app.go
   ```
5. Run application:
   ```bash
   ./app
   ```

### With Docker

1. Install `docker 20.10.14+`
2. Run tests:
   ```bash
   ./run-tests.sh
   ```
3. Build application:
   ```bash
   docker build . -t gogol:1.0.0
   ```
4. Run application:
   ```bash
   docker run -p 8080:8080 gogol:1.0.0
   ```
